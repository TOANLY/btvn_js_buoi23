//Bài 1: Tính tiền lương nhân viên
function tinhLuong() {
    //input
    var luong1Ngay = 100000;
    var soNgayLam = document.getElementById('soNgayCong').value *1;         
    //xử lý                                              
    console.log({soNgayLam});
    var tongLuong = luong1Ngay * soNgayLam;
    //output
    document.getElementById('tongLuong').innerText = `Tiền lương tháng này là ${tongLuong}`;
}
//Bài 2: Tính giá trị trung bình 
function trungBinh() {
    //input
    var soThuNhat = document.getElementById('soThuNhat').value*1;
    var soThuHai = document.getElementById('soThuHai').value*1;
    var soThuBa = document.getElementById('soThuBa').value*1;
    var soThuTu = document.getElementById('soThuTu').value*1;
    var soThuNam = document.getElementById('soThuNam').value*1;
    var soKySo = 5;
    //xử lý 
    console.log({soThuNhat,soThuHai,soThuBa,soThuTu,soThuNam});
    var trungBinh = (soThuNhat + soThuHai + soThuBa + soThuTu + soThuNam) / soKySo;
    //output
    document.getElementById('trungBinh').innerText = `gias trị trung bình là ${trungBinh}`;
}
//Bài 3: Quy đổi tiền 
function doiTien() {
    //input 
    var triGiaVND = 23500;
    var soTienCanDoi = document.getElementById('soTienCanDoi').value*1;
    // xử lý 
    console.log({soTienCanDoi});
    var doiTien = triGiaVND * soTienCanDoi; 
    //output 
    document.getElementById('doiTien').innerText = `giá trị tiền Vệt Nam là ${doiTien} VND`;
}

//Bài 4: Tính chu vi & diện tích
function ketQua() {
    //input
    var chieuDai = document.getElementById('chieuDai').value*1;
    var chieuRong = document.getElementById('chieuRong').value*1;

    // xử lý
    console.log({chieuDai,chieuRong});
    var dienTich = chieuDai * chieuRong;
    var chuVi = (chieuDai + chieuRong) *2;

    //output 
    document.getElementById('dienTich').innerText = `Diện tích hcn là ${dienTich}`;
    document.getElementById('chuVi').innerText = `Chu vi hcn là ${chuVi}`;
}
// Bài 5: Tính tổng 2 ký số 
function tong() {
    //input
    var kySo = document.getElementById('kySo').value*1;
    // xử lý 
    console.log({kySo});
    var hangDonVi = kySo % 10;
    var hangChuc = (kySo - hangDonVi) / 10;
    var tong = hangChuc + hangDonVi;
    //output
    document.getElementById('tong').innerText = `tổng 2 ký số là ${tong}`;
}